from os import listdir
from os.path import isfile, join
from random import randint
import socket

HOST = '127.0.0.1'
PORT = 65432
arenas_dir = "arenas"
arena_len_x = 5
arena_len_y = 5


class MapItem:
    def __init__(self, obj_type, x_location, y_location):
        self.obj_type = obj_type
        self.x_location = x_location
        self.y_location = y_location

    def get_location(self):
        return [self.x_location, self.y_location]

    def set_location(self, x, y):
        self.x_location = x
        self.y_location = y


class Map:
    def __init__(self):
        self.my_map = []

    def set_my_map(self, arena_map_from_file):
        for line in range(5):
            self.my_map.append([])
            for tab in range(5):
                if int(arena_map_from_file[5 * line + tab]):
                    # True for wall
                    self.my_map[line].append(True)
                else:
                    # False for empty
                    self.my_map[line].append(False)

    def get_my_map(self):
        return self.my_map

    def to_string_map(self, cop, player, treasure):
        to_return = []
        for line in self.my_map:
            for tab in line:
                if tab:
                    to_return.append("1")
                else:
                    to_return.append("0")
        to_return[cop.get_location()[0] + arena_len_x * (cop.get_location()[1] - 1)] = "c"
        to_return[player.get_location()[0] + arena_len_x * (player.get_location()[1] - 1)] = "p"
        to_return[treasure.get_location()[0] + arena_len_x * (treasure.get_location()[1] - 1)] = "t"
        return to_return

    def to_string_map_string(self, cop, player, treasure):
        to_return = ""
        for line in self.my_map:
            for tab in line:
                if tab:
                    to_return += "1"
                else:
                    to_return += "0"
        to_return = to_return[:cop.get_location()[0] + arena_len_x * (cop.get_location()[1] - 1)] + "c" + to_return[
                                                                                                cop.get_location()[
                                                                                                    0] + arena_len_x * (
                                                                                                        cop.get_location()[
                                                                                                            1] - 1) + 1:]
        to_return = to_return[:player.get_location()[0] + arena_len_x * (player.get_location()[1] - 1)] + "p" + to_return[
                                                                                                      player.get_location()[
                                                                                                          0] + arena_len_x * (
                                                                                                              player.get_location()[
                                                                                                                  1] - 1) + 1:]
        to_return = to_return[:treasure.get_location()[0] + arena_len_x * (treasure.get_location()[1] - 1)] + "t" + to_return[
                                                                                                          treasure.get_location()[
                                                                                                              0] + arena_len_x * (
                                                                                                                  treasure.get_location()[
                                                                                                                      1] - 1) + 1:]
        return to_return


def format_message(message_to_format):
    message_to_format_len = len(message_to_format)
    if message_to_format_len <= 9:
        return "000" + str(message_to_format_len) + message_to_format
    elif message_to_format_len <= 99:
        return "00" + str(message_to_format_len) + message_to_format
    elif message_to_format_len <= 999:
        return "0" + str(message_to_format_len) + message_to_format
    else:
        return str(message_to_format_len) + message_to_format


def get_list_of_arena_files(path):
    all_files = [f for f in listdir(path) if isfile(join(path, f))]
    new_message = ""
    file_counter = 1
    for file_listed in all_files:
        new_message += "$" + str(file_counter) + "," + str(file_listed)
        file_counter += 1
    return new_message[1:]


def create_random_location(len_x, len_y, map_item, my_map):
    place_x, place_y = randint(0, len_x), randint(0, len_y)
    if my_map[place_y][place_x]:
        create_random_location(len_x, len_y, map_item, my_map)
    else:
        map_item.set_location(place_x, place_y)


def main():
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    my_socket.bind((HOST, PORT))
    my_socket.listen()
    print("Server up and running!")

    new_message = ""

    conn, address = my_socket.accept()
    print(f"Client found: {address}")

    cop = MapItem("cop", -1, -1)
    treasure = MapItem("treasure", -1, -1)
    player = MapItem("player", -1, -1)
    my_map = Map()

    while True:
        data_len = conn.recv(4)
        if not data_len:
            break
        data_len = data_len.decode()
        received_data = conn.recv(int(data_len)).decode()
        print("received data:  " + str(received_data))

        if received_data == "LIST_ARENAS":
            new_message = get_list_of_arena_files(arenas_dir)

        elif received_data[0:12] == "CHOOSE_ARENA":
            chosen_arena_num = int(received_data[12:])
            arenas_list = [f for f in listdir(arenas_dir) if isfile(join(arenas_dir, f))]
            arena_map_from_file = open(f"{arenas_dir}\{arenas_list[chosen_arena_num - 1]}", "r").read()
            my_map.set_my_map(arena_map_from_file)

            current_map_initial = my_map.get_my_map()
            create_random_location(arena_len_x - 1, arena_len_y - 1, cop, current_map_initial)
            create_random_location(arena_len_x - 1, arena_len_y - 1, treasure, current_map_initial)
            create_random_location(arena_len_x - 1, arena_len_y - 1, player, current_map_initial)

            new_message = my_map.to_string_map_string(cop, treasure, player)

        elif received_data[0:4] == "MOVE":
            new_message = "failed. hit a wall."
            move = received_data.split(" ")[1]

            if move == "UP":
                if player.get_location()[1] >= 1:
                    if not my_map.get_my_map()[player.get_location()[1] - 1][player.get_location()[0]]:
                        player.set_location(player.get_location()[0], player.get_location()[1] - 1)
                        print("up")
                        new_message = "up"
            elif move == "DOWN":
                if player.get_location()[1] < arena_len_y - 1:
                    if not my_map.get_my_map()[player.get_location()[1] + 1][player.get_location()[0]]:
                        player.set_location(player.get_location()[0], player.get_location()[1] + 1)
                        print("down")
                        new_message = "down"
            elif move == "RIGHT":
                if player.get_location()[0] < arena_len_x - 1:
                    if not my_map.get_my_map()[player.get_location()[1] + 1][player.get_location()[0]]:
                        player.set_location(player.get_location()[0] + 1, player.get_location()[1])
                        print("right")
                        new_message = "right"
            elif move == "LEFT":
                if player.get_location()[0] >= 1:
                    if not my_map.get_my_map()[player.get_location()[1] - 1][player.get_location()[0]]:
                        player.set_location(player.get_location()[0] - 1, player.get_location()[1])
                        print("left")
                        new_message = "left"

            # TODO moving randomly cop (check wall)
            cop_move = randint(0, 3)
            if cop_move == "0":
                if cop.get_location()[1] >= 1:
                    if not my_map.get_my_map()[cop.get_location()[1] - 1][cop.get_location()[0]]:
                        cop.set_location(cop.get_location()[0], cop.get_location()[1] - 1)
                        print("cop_up")
            elif cop_move == "2":
                if cop.get_location()[1] < arena_len_y - 1:
                    if not my_map.get_my_map()[cop.get_location()[1] + 1][cop.get_location()[0]]:
                        cop.set_location(cop.get_location()[0], cop.get_location()[1] + 1)
                        print("cop_down")
            elif cop_move == "1":
                if cop.get_location()[0] < arena_len_x - 1:
                    if not my_map.get_my_map()[cop.get_location()[1] + 1][cop.get_location()[0]]:
                        cop.set_location(cop.get_location()[0] + 1, cop.get_location()[1])
                        print("cop_right")
            elif cop_move == "3":
                if cop.get_location()[0] >= 1:
                    if not my_map.get_my_map()[cop.get_location()[1] - 1][cop.get_location()[0]]:
                        cop.set_location(cop.get_location()[0] - 1, cop.get_location()[1])
                        print("cop_left")

            if treasure.get_location()[0] == player.get_location()[0] and treasure.get_location()[1] == player.get_location()[1]:
                new_message = "WON"
            if cop.get_location()[0] == player.get_location()[0] and cop.get_location()[1] == player.get_location()[1]:
                new_message = "LOOSE"

            print(player.get_location())

        elif received_data == "STATUS":
            new_message = my_map.to_string_map_string(cop, player, treasure)

        else:
            print("???")
            # ???

        conn.sendall(format_message(str(new_message)).encode())


if __name__ == '__main__':
    main()
