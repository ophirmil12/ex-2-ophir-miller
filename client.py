import socket

HOST = '127.0.0.1'
PORT = 65432
x_len = 5


def format_message(message_to_format):
    len_message_to_format = len(message_to_format)
    if len_message_to_format <= 9:
        return "000" + str(len_message_to_format) + message_to_format
    elif len_message_to_format <= 99:
        return "00" + str(len_message_to_format) + message_to_format
    elif len_message_to_format <= 999:
        return "0" + str(len_message_to_format) + message_to_format
    else:
        return str(len_message_to_format) + message_to_format


def asking_arenas(my_socket):
    is_want_list = input("Would you like to see arenas? \nEnter `yes` or `no`:    ").lower()
    if is_want_list == "yes":
        my_socket.sendall(format_message("LIST_ARENAS").encode())
        data_len = my_socket.recv(4).decode()
        received_data = my_socket.recv(int(data_len)).decode()
        return True, received_data
    elif is_want_list == "no":
        return False, None
    else:
        print("Error! enter correct values!")
        # I know recursive function isn`t the best...
        asking_arenas(my_socket)


def input_chosen_arena():
    chosen_arena = input("chosen arena:  ")
    try:
        chosen_arena = int(chosen_arena)
        return chosen_arena
    except:
        print("int required!")
        return input_chosen_arena()


def check_if_in_range_arenas(chosen_arena, arenas_received_data):
    try:
        return arenas_received_data[chosen_arena - 1].split(",")[0]
    except:
        return check_if_in_range_arenas(input_chosen_arena(), arenas_received_data)


def send_chosen_arena(chosen_arena, my_socket):
    my_socket.sendall(format_message(f"CHOOSE_ARENA{str(chosen_arena)}").encode())


def handle_game_move(new_message, my_socket):
    did_win, did_loose = False, False
    my_socket.sendall(format_message(str(new_message)).encode())
    data_len = my_socket.recv(4).decode()
    received_data = my_socket.recv(int(data_len)).decode()
    if received_data == "WON":
        did_win = True
    elif received_data == "LOOSE":
        did_loose = True
    else:
        print(f"moved {received_data}")
    return did_win, did_loose


def handle_game_status(my_socket, new_message):
    my_socket.sendall(format_message(str(new_message)).encode())
    data_len = my_socket.recv(4).decode()
    received_data = my_socket.recv(int(data_len)).decode()
    map_status = []
    for line in range(x_len):
        map_status.append(received_data[line*5:line*5+5])
    return map_status


def main():
    print("""Welcome to the game!
    By OPHIR MILLER.
    Let`s play!
    Choose arena from the following: \n""")
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    my_socket.connect((HOST, PORT))
    is_want, arenas_received_data = asking_arenas(my_socket)
    if is_want:
        arenas_received_data = arenas_received_data.split("$")
        print(arenas_received_data)
        chosen_arena = check_if_in_range_arenas(input_chosen_arena(), arenas_received_data)
        send_chosen_arena(chosen_arena, my_socket)
    else:
        chosen_arena = check_if_in_range_arenas(input_chosen_arena(), arenas_received_data)
        # TODO check problem with "no" command
        send_chosen_arena(chosen_arena, my_socket)

    data_len = my_socket.recv(4).decode()
    initial_map = my_socket.recv(int(data_len)).decode()
    # TODO print initial map nicely (can make function from printing)
    print("Game Start!")
    print("Possible moves: UP, DOWN, RIGHT, LEFT, STATUS")

    while True:
        command = input("Enter your move or STATUS:   ")
        if command == "STATUS":
            new_message = command
            map_status = handle_game_status(my_socket, new_message)
            print("* " * 2 * x_len + "* *")
            for line in map_status:
                print("* ", end="")
                for tab in line:
                    if tab == "1":
                        print("#   ", end="")
                    elif tab == "0":
                        print("    ", end="")
                    else:
                        print(tab + "   ", end="")
                print("*", end="")
                print("")
            print("* " * 2 * x_len + "* *\n")

        elif command == "UP" or command == "DOWN" or command == "RIGHT" or command == "LEFT":
            new_message = f"MOVE {command}"
            did_win, did_loose = handle_game_move(new_message, my_socket)
            if did_win:
                print("You won! Good job!")
                break
            elif did_loose:
                print("You lost... try again letter.")
                break
            else:
                pass

        else:
            print("Move illegal!")


if __name__ == '__main__':
    main()
